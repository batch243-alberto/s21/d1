// console.log("Happy monday")

// An array in programming is simply a list of data. Data that are related/connected with each other;

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924"; 
let studentNumberC = "2020-1925"; 
let studentNumberD = "2020-1926"; 
let studentNumberE = "2020-1927"; 

// Now, with anarray we can simply write the code above like this;

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

	// [Section] Array 
	/*
		-arrays are used to store multiple related values in a single variable
		-they are declared using square brackets([]) also know "Array Literals".
		- commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks.
		- array also provide access a number of function/ metthods that helop achieveing specific task.
		- a method is another term for functions associated with an object/ array and is used to execute statements that are relevant
		- majority of methods are used to manipulate information stored within the same object. 
		- arrays are also object which is another type

		Syntax:
			let/const arrayName = [elementA, elementB, elementC ...]
	*/

// Xcommone examples of arrays
let grades= [98.5, 94.4, 89.2, 90.1];
console.log(grades);
let computerBrands = ["Acer", "Asus", "Lenovo", "Dell", "Mac", "Samsung"];

console.log(computerBrands); 

// Possible use of an Array but is not recommended.
let mixedArr = [12, "Asus", null, undefined, {}];
console.log(mixedArr);
// Alternative way to write arrays

/*console.log(myTasks);
console.log(myTasks[1].length);*/

// Create an array with values from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";

let cities = [city1, city2, city3];

console.log(cities);

// [Section] length property
// The .length property(for objects) allows us to get and set the total number of items in an array.

console.log(typeof grades.length);
console.log(cities.length);

// Will return a zero
let blankArr=[];
console.log(blankArr.length);

// Undefined, because we only intialize but not declare variable
let array;
console.log(array);

// length property can also be used with strings. Some array methods and properties can also be used w/ strings

// spaces will also be counted
let fullName = "John Doe";
console.log(fullName.length);

// length property on strings shows the number of characters in a string. Spaces are counted as character in strings.

// length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array. 

let myTasks = [
	"drink html", 
	"eat javascript", 
	"inhale css", 
	"bake sass"];
console.log(myTasks)

myTasks.length = myTasks.length-1;
console.log(myTasks)

// To delete a specific item in array we can employ array methods (which will be shown in the next session) or an algorithm set of code to process tasks.

// another example using decrementation 
cities.length--;
console.log(cities)

// we can't do the same on the strings:
console.log(fullName);
fullName.length = fullName.length-1;
console.log(fullName);
console.log(fullName.length);

// if you can shorten the array by setting the length of property, you can also lengthen it by adding number into the length property.

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length = theBeatles.length + 1;
console.log(theBeatles); 

// because of type coercion it only concatenates
/*theBeatles += "Roda"
console.log(theBeatles);
*/

// Best practice to put the empty
theBeatles[4] = "Roda";
console.log(theBeatles);
theBeatles.length = theBeatles.length + 1;
theBeatles[5] = "rushtin";
console.log(theBeatles);  


// [Section] Reading/ accession elements of arrays
	// Accessing array elements is one of the more common task that we do with an array
	// This can be donw through the use of index
	// Each element in an array is associated with it's own index/number.

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[4]);

// We can also save/store array elements in another variable; 
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// We can also reassign array values using items indices. 

console.log("Array before the reassignment");
console.log(lakersLegends)

lakersLegends[2] = "Pau Gasol";
console.log("Array after the reassignment");
console.log(lakersLegends);

// Accessing the last element of an array
// Since te first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element

let lastElementIndex = lakersLegends.length-1;
console.log(lakersLegends[lastElementIndex]);

// Adding items into the array without using array methods
// length starts at 1 while index starts at 0 
let newArr = []
newArr[0] = "Cloud Strife";
console.log(newArr);
newArr[newArr.length] = "Tifa Lockhart"; 
console.log(newArr);

// You can also add at the end of the array. Instead of adding it in the front to avoid the risk of replacing the first items in the array.
newArr[newArr.length] = "Barret Wallace";
console.log(newArr)

// looping over an array
// you can use a for loop to iterate over all items in an array 

	for(let index = 0; index < newArr.length; index++){
		console.log(newArr[index])
	}

let numArr = [5, 12, 30, 46, 40];

// We are going to create a for loop, wherein it will check whether the element/ numners are divisible by 5
	for(let index = 0; index <= numArr.length-1; index++){
		if(numArr[index]% 5 === 0){
			console.log(numArr[index] + " is divisible by 5")
		}
		else{
			console.log(numArr[index] + " is not divisible by 5")
		}
	}

// [Section] Multidimensional Arrays
	// Mutlidimensional arrays are useful for storing complex data structures. 
	// a practical application of this is to help visualize/create real world objects
	// Though useful in a number, creating complex array structure is not always recommended

let chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"], 
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
]	

console.table(chessBoard);
console.log(chessBoard[4][5]);
console.log(chessBoard[7][3]);

